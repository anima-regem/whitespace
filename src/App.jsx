import { useState } from 'react'
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from './Pages/Home/Home'
import Exhibition from './Pages/Exhibition/Exhibition'
import './App.css'

function App() {

  return (
    <div>
      <BrowserRouter>
      <Routes>
        
        <Route exact path="/" element={<Home/>} />
        <Route path="/:id" element={<Exhibition/>} />
      </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App
