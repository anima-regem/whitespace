import { useParams } from "react-router-dom";
import "./Exhibition.css";
import design from "../../assets/images/poster.svg";
import logo from "../../assets/images/logo.svg";
import { data } from "../../data";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
const Exhibition = (props) => {
  const navigate = useNavigate();
  const { id } = useParams();
  return (
    <div>
      <nav className="navbar">
        <div className="container-fluid navbar-div">
          <a className="navbar-brand" href="/">
            <img
              src={logo}
              alt="Logo"
              width="50"
              height="50"
              className="d-inline-block align-text-center"
            />
            <canvas id="canvas" hidden></canvas>
            <span className="navtext">whitespace</span>
          </a>
        </div>
      </nav>
      <article className="container-fluid">
        <div className="wrap">
          <div className="main-body">
            <div className="hero-text">Whitespace</div>
          </div>
        </div>
      </article>
      <article className="container-fluid">
        <div className="wrap">
          <div className="main-body">
            <div className="hero-text exhibition-hero">exhibition</div>
            <div className="content">
              Whitespace is a community of designers inside students started at
              Government Engineering College, Palakkad. Our goal is to provide a
              setting for peer-to-peer learning where individuals may learn,
              share knowledge,collaborate, and explore their own creative
              potential. The community is always open for design enthusiasts
            </div>
          </div>
        </div>
      </article>
      <main>
        <div className="bth">
          <div className="footer-text">
            <span className="bth-text">behind the scenes</span>
          </div>
          <hr />
          <div className="showcase">
            <img src={data[id].imgLnk} id="source" alt="" />
            <a
              href={data[id].imgLnk}
              download={"whitespace_"+id}
              id="downloader"
              hidden
            ></a>
          </div>
          <div className="download w-100 container-fluid text-center">
            <button
              className="button-18"
              role="button"
              onClick={() => document.getElementById("downloader").click()}
            >
              <i className="fa-solid fa-cloud-arrow-down">
                <span className="dwnld-txt"> download</span>
              </i>
            </button>
          </div>

          <div className="footer-text">
            <span className="more-data">the art and the artist</span>
          </div>
          <hr />
          <div className="more">
            <div className="art">{data[id].art}</div>
            <div className="summary">{data[id].summary}</div>
            <br />
            <br />
            curated by,
            <br />
            <br />
            <span className="artist">{data[id].artist}</span>
            <br />
            <span className="tag">@{data[id].tag}</span>
          </div>
        </div>
      </main>
      <footer>
        <div className="footer-text">
          <span>whitespace</span>
        </div>
        <hr />
        <div className="social">
          <div className="insta d-flex">
            <i className="fa-brands fa-instagram">
              <a href="https://www.instagram.com/whitespace.ui/">
                whitespace.ui
              </a>
            </i>
          </div>
          <div className="linkdn">
            <i className="fa-brands fa-linkedin">
              <a href="">whitespace.ui</a>
            </i>
          </div>
        </div>
      </footer>
    </div>
  );
};
export default Exhibition;
