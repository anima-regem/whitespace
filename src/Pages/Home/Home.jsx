import "./Home.css";
import design from "../../assets/images/poster.svg";
import logo from "../../assets/images/logo.svg";

function Home() {
  return (
    <div>
      <nav className="navbar">
        <div className="container-fluid navbar-div">
          <a className="navbar-brand" href="/">
            <img
              src={logo}
              alt="Logo"
              width="50"
              height="50"
              className="d-inline-block align-text-center"
            />
            <span className="navtext">whitespace</span>
          </a>
        </div>
      </nav>
      <article className="container-fluid">
        <div className="wrap">
          <div className="main-body">
            <div className="hero-text">Whitespace</div>
            <div className="content">
              “WHITESPACE”- THE SPACE CREATED FOR DESIGNERS.
              <br />
              <br />
              Whitespace is a community of designers inside students started at
              Government Engineering College, Palakkad. Our goal is to provide a
              setting for peer-to-peer learning where individuals may learn,
              share knowledge,collaborate, and explore their own creative
              potential. The community is always open for design enthusiasts
            </div>
            <div className="img-section text-center">
              <img src={design} alt="" />
            </div>
          </div>
        </div>
      </article>
      <footer>
        <div className="footer-text">
          <span>whitespace</span>
        </div>
        <hr />
        <div className="social">
          <div className="insta d-flex">
            <i class="fa-brands fa-instagram">
              <a href="https://www.instagram.com/whitespace.ui/">whitespace.ui</a>
            </i>
          </div>
          <div className="linkdn">
            <i class="fa-brands fa-linkedin">
              <a href="">whitespace.ui</a>
            </i>
          </div>
        </div>
      </footer>
    </div>
  );
}

export default Home;
