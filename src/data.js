export const data = {
  oibn: {
    id: 1,
    imgLnk: "images/whitespace_1.png",
    art: "Pride and Prejudice",
    summary:
      "Pride is a feeling of deep satisfaction and accomplishment, typically in relation to one's own achievements or abilities. It is a positive and constructive emotion that can motivate people to strive for excellence and achieve their goals. In contrast, prejudice is an irrational and unfounded belief that one group of people is superior to another. It often leads to negative attitudes and discriminatory behavior towards members of the targeted group.",
    artist: "Arya C S",
    tag: "arya_c_s._",
  },
  qofr: {
    id: 2,
    imgLnk: "images/whitespace_2.png",
    art: "Weirdo",
    summary:
      "Being weird is about being unique and different from the norm. It means thinking and acting in ways that are unconventional or unexpected. This can be a positive trait, as it allows people to bring new perspectives and ideas to the table. Thinking in a different way can help people to see the world from a fresh perspective and come up with creative solutions to problems.",
    artist: "Vaishakh Suresh",
    tag: "soulcastor",
  },
  oixz: {
    id: 3,
    imgLnk: "images/whitespace_3.png",
    art: "Lost in a different world",
    summary:
      "Time is a precious and limited resource. Once it is spent, it cannot be recovered or regained. This is why it is so important to use our time wisely and avoid wasting it on activities or pursuits that do not bring us value or fulfillment. Wasting time can lead to regret and a sense of missed opportunities. It can also prevent us from achieving our goals and living the life we want. Therefore, it is important to prioritize our time and focus on the things that matter most to us. This can help us to make the most of our lives and avoid the regret of wasted time.",
    artist: "Nandana V S",
    tag: "nandnna._",
  },
  qbtu: {
    id: 4,
    imgLnk: "images/whitespace_4.png",
    art: "Wisdom",
    summary:
      "Knowledge is the understanding and information that we acquire through study, experience, or instruction. It is the facts, principles, and concepts that we learn and apply in our daily lives. However, knowledge alone does not guarantee good judgment or effective decision making. Wisdom, on the other hand, is the ability to apply knowledge and experience in a thoughtful and insightful way. It is the quality of being wise, or having good judgment and common sense. Wisdom is not just about knowing things, but about understanding how to use that knowledge to make sound decisions and navigate the complexities of life. Wisdom is the ability to listen and consider different perspectives, to think critically and reflectively, and to make choices that are beneficial not just for ourselves, but for others as well.",
    artist: "Nandana V S",
    tag: "nandnna._",
  },
  zzpf: {
    id: 5,
    imgLnk: "images/whitespace_5.png",
    art: "Mistakes",
    summary:
      "Making mistakes is an inevitable part of life. Everyone makes mistakes, and they can be valuable learning opportunities. Mistakes can teach us about our own limitations and help us to improve and grow. They can also help us to be more resilient and better equipped to handle challenges in the future. Therefore, it is important to embrace mistakes and not be afraid to make them. Instead of avoiding or fearing mistakes, we should view them as opportunities to learn and improve. By being a mistake maker, we can learn from our mistakes and use them to become better and wiser.",
    artist: "Akhil Krishna P",
    tag: "jakeandtheboy",
  },
  chae: {
    id: 6,
    imgLnk: "images/whitespace_6.png",
    art: "Point of view",
    summary:
      "Imagination is the ability to form new ideas, images, and concepts in the mind. It is the power of the mind to create, innovate, and think beyond the boundaries of reality. Imagination is a valuable and essential quality that can help us to see the world in new and different ways. It can help us to solve problems, express ourselves creatively, and explore new ideas and possibilities. Finding your point of view means developing and expressing your own unique perspective and opinions on the world. It means having the confidence and courage to think for yourself and to express your thoughts and ideas clearly and authentically. Your point of view is shaped by your experiences, values, and beliefs, and it can help you to see the world in a way that is meaningful and significant to you. By finding and expressing your point of view, you can contribute to the diversity of ideas and perspectives in the world, and enrich the conversations and discussions that take place around you.",
    artist: "Akhil Krishna P",
    tag: "jakeandtheboy",
  },
  uqsf: {
    id: 7,
    imgLnk: "images/whitespace_7.png",
    art: "Balance",
    summary:
      "This statement suggests that life is dynamic and constantly changing. Just like a bicycle, we must keep moving and adapting in order to maintain our balance and avoid falling. If we stop moving and trying new things, we risk losing our balance and falling behind. Therefore, it is important to keep learning and growing, and to take risks and pursue new opportunities in order to keep moving forward in life. By maintaining a forward momentum, we can stay balanced and on track, and avoid getting stuck or losing our way.    ",
    artist: "Akhil Krishna P",
    tag: "jakeandtheboy",
  },
  muak: {
    id: 8,
    imgLnk: "images/whitespace_8.png",
    art: "Distraction",
    summary:
      "Distractions are anything that takes our attention away from what we are currently doing. They can be external, such as loud noises or interruptions from other people, or internal, such as our own thoughts or emotions. Distractions can be helpful in some cases, as they can provide a break or a change of pace that can refresh our minds and help us to stay focused. However, in other cases, distractions can be detrimental, as they can disrupt our concentration and prevent us from completing tasks or achieving our goals. We have the ability to control whether or not we allow distractions to impact us. We can choose to focus on the task at hand and ignore distractions, or we can allow ourselves to be easily swayed by them. By being aware of our own susceptibility to distractions, we can take steps to minimize their impact and stay focused on what is important. This can help us to be more productive and successful in our endeavors.",
    artist: "Akhil Krishna P",
    tag: "jakeandtheboy",
  },
  gzxp: {
    id: 9,
    imgLnk: "images/whitespace_9.png",
    art: "Memories",
    summary:
      "Life is a mixture of ups and downs, and we will experience both joy and sadness. Tears are a natural response to sadness, pain, or grief, and they can provide emotional release and catharsis. Smiles are a sign of happiness, joy, and contentment, and they can light up our faces and make us feel good. Both tears and smiles are temporary, fleeting moments that come and go. However, memories are more lasting and enduring. They are the mental pictures and recollections of our experiences, and they can provide us with a sense of connection to the past. Memories can be positive, such as happy moments with loved ones, or negative, such as difficult or painful experiences. Regardless of their nature, memories can stay with us for a long time, and they can shape who we are and how we see the world. Therefore, it is important to cherish and hold on to our memories, as they are a valuable part of our lives.",
    artist: "Akhil Krishna P",
    tag: "jakeandtheboy",
  },
  azli: {
    id: 10,
    imgLnk: "images/whitespace_10.png",
    art: "Mistakes",
    summary:
      "Faking perfection, is about trying to present ourselves as flawless and without mistakes. It is the act of hiding our mistakes and flaws, and pretending that we are perfect. However, this is not only unrealistic, but also unhealthy. Faking perfection puts a lot of pressure on ourselves, and can lead to stress, anxiety, and a lack of self-acceptance. It can also prevent us from learning and growing, and from being authentic and genuine with others. Therefore, it is better to embrace our mistakes and learn from them, rather than trying to fake perfection. By making new mistakes, we can continue to learn and grow, and become the best versions of ourselves.",
    artist: "Akhil Krishna P",
    tag: "jakeandtheboy",
  },
  rpao: {
    id: 11,
    imgLnk: "images/whitespace_11.png",
    art: "Cyberbullying",
    summary:
      "Cyberbullying is the use of technology, such as the internet or smartphones, to deliberately and repeatedly harm or harass others. It can take many forms, such as sending threatening or harassing messages, spreading rumors or false information, or sharing embarrassing or private photos or videos without permission. Cyberbullying can have serious consequences, such as causing emotional distress, damage to reputation, or even physical harm. Online interactions are not the same as real-life interactions, and people should be aware of the potential dangers of cyberbullying. Online, people can present themselves in any way they want, and they may not always be who they appear to be. Therefore, it is important to be cautious and mindful of the potential risks of online interactions, and to not take everything at face value. By recognizing that online is not your reality, you can avoid being a victim of cyberbullying, and protect yourself and others from the harmful effects of online harassment.",
    artist: "Nanadana V S",
    tag: "nandnna._",
  },
  kyya: {
    id: 12,
    imgLnk: "images/whitespace_12.png",
    art: "Women",
    summary:
      "Just like a tea bag, a woman may not appear strong or powerful at first glance. However, when she is put in a difficult or challenging situation, her true strength and resilience can be revealed. Just as a tea bag releases its flavor and strength when it is steeped in hot water, a woman can show her inner strength and capabilities when she is faced with adversity. This design is intended to be empowering and to recognize the strength and resilience of women. It suggests that women are often underestimated and undervalued, but that they are capable of overcoming challenges and achieving great things. By recognizing the strength of women, we can challenge gender stereotypes and biases, and create a more equitable and inclusive society.",
    artist: "Nanadana V S",
    tag: "nandnna._",
  },
  dqsp: {
    id: 13,
    imgLnk: "images/whitespace_13.png",
    art: "Smoking kills",
    summary:
      "Smoking is a highly addictive behavior that can have serious health consequences. One of the most well-known risks of smoking is its potential to cause lung cancer and other respiratory diseases. This design is a warning about the dangers of smoking. It emphasizes that the craving for a cigarette is only temporary, but the damage to the lungs is permanent. This is intended to encourage smokers to quit, and to highlight the long-term consequences of smoking. By quitting smoking, people can reduce their risk of developing lung cancer and other serious diseases, and improve their overall health and well-being.",
    artist: "Nanadana V S",
    tag: "nandnna._",
  },
  iaqc: {
    id: 14,
    imgLnk: "images/whitespace_14.png",
    art: "Thought and action",
    summary:
      "Thought and action are two related but distinct concepts. Thought refers to the mental process of generating, processing, and storing information. It is the conscious or unconscious process of thinking, reasoning, and imagining. Action, on the other hand, refers to the act of doing or performing something. It is the physical or observable manifestation of thought. Thought and action are interconnected, as our thoughts often drive our actions. Our thoughts can influence our behavior, and our actions can reflect our thoughts.",
    artist: "Viswanatha Kartha V",
    tag: "viswanathakarthav",
  },
  txae: {
    id: 15,
    imgLnk: "images/whitespace_15.png",
    art: "Dreams",
    summary:
      "Believing in your dreams means having faith and confidence in the things that you want to accomplish or achieve. It means having a positive and optimistic attitude towards your goals, and being willing to work hard and persevere in order to make them a reality. Believing in your dreams is an essential quality for success, as it gives you the motivation and determination to overcome obstacles and challenges. Having belief in your dreams is also important for mental and emotional well-being. It can help you to feel more positive and hopeful, and to cope with stress and adversity. It can also provide you with a sense of purpose and direction, and can help you to feel more fulfilled and satisfied with your life. Therefore, it is important to believe in your dreams, and to nurture and cultivate that belief in order to achieve your goals and live a happy and fulfilling life.",
    artist: "Viswanatha Kartha V",
    tag: "viswanathakarthav",
  },
  wwsz: {
    id: 16,
    imgLnk: "images/whitespace_16.png",
    art: "Link of Paradise",
    summary:
      "Just as a paw print is a visible reminder of the presence of a dog, the love and companionship of a dog can leave a lasting impression on our hearts. Dogs are known for their loyalty, affection, and devotion, and they can bring joy and happiness to our lives. This design is intended to be a tribute to the special bond that exists between humans and dogs. It recognizes the positive impact that dogs can have on our emotional and mental well-being, and the ways in which they can enrich our lives. By appreciating the paw prints that dogs leave on our hearts, we can honor the important role that they play in our lives, and the lasting impact that they have on us.",
    artist: "Mohammed Ashiq Rahman",
    tag: "seanonyms",
  },
  jvkh: {
    id: 17,
    imgLnk: "images/whitespace_17.png",
    art: "Journey",
    summary:
      "NEThis statement is about the importance of letting relationships and connections develop naturally, rather than trying to force them. It suggests that trust in the process, or the natural course of events, is key to forming meaningful and lasting connections. Forcing a connection means trying to make something happen or happen faster than it naturally would. This can be done through manipulation, coercion, or other tactics that are intended to push the relationship forward. However, these tactics are often ineffective, and can even damage the relationship. They can create resentment, distrust, and discomfort, and can prevent the relationship from developing in a healthy and organic way. On the other hand, trusting the process means allowing the relationship to unfold and develop at its own pace. It means being patient, open-minded, and receptive to the other person, and allowing the relationship to grow naturally. This can create a more authentic and genuine connection, and can lay the groundwork for a strong and lasting relationship. Therefore, it is better to trust the process and let the relationship develop naturally, rather than trying to force a connection.",
    artist: "Mohammed Ashiq Rahman",
    tag: "seanonyms",
  },
  mavu: {
    id: 18,
    imgLnk: "images/whitespace_18.png",
    art: "Search",
    summary:
      "People who are loyal and faithful often endure the most pain and hardship in relationships. Loyalty is a noble and admirable quality, but it can also make people vulnerable to being taken advantage of or hurt. Overthinking is a common problem that can cause stress, anxiety, and mental fatigue. It can prevent people from enjoying the present moment, and can make them miss out on opportunities and experiences. This design is a reminder that it is important to take breaks from overthinking, and to find ways to calm and quiet the mind. It is more important to focus on becoming a valuable and valuable person, rather than being constantly available and accommodating to others. By being valuable, we can add value to our relationships and to the world around us. This can help us to feel more fulfilled and satisfied with our lives, and to build stronger and more meaningful connections with others.",
    artist: "Mohammed Ashiq Rahman",
    tag: "seanonyms",
  },
  qfkg: {
    id: 19,
    imgLnk: "images/whitespace_19.png",
    art: "OLD AND NEW",
    summary:
      "By looking back at the past, we can learn and gain valuable insights that can help us to understand and navigate the present and the future. The past is a vast storehouse of knowledge and experience, and by studying and analyzing it, we can learn about the successes and failures of those who came before us. This can provide us with valuable lessons and perspective, and can help us to make better decisions and avoid repeating the mistakes of the past. Furthermore, by viewing the old, we can also appreciate the continuity and progression of history, and gain a sense of connection and belonging to the larger story of human experience. By understanding the past, we can better understand our place in the world, and the context and significance of our own experiences. Therefore, it is important to view the old, and to learn from it, in order to better understand and navigate the new.",
    artist: "Fathima Laya",
    tag: "fathima_laya_",
  },
  wxmj: {
    id: 20,
    imgLnk: "images/whitespace_20.png",
    art: "Design",
    summary:
      "Design is not just an abstract or theoretical concept, but something that has real and practical applications in everyday life. Design is the art and science of creating functional and aesthetically pleasing objects, systems, or environments. It is a process that involves creativity, problem-solving, and innovation, and that aims to make things better, more efficient, and more enjoyable to use. Therefore, this is a reminder that design is not just a philosophical or theoretical pursuit, but something that has a tangible and tangible impact on our lives. Good design can improve the quality of our lives, and can make our experiences more enjoyable, efficient, and satisfying. It can make our surroundings more pleasant and functional, and can provide us with tools and technologies that can make our lives easier and more productive. By recognizing the value of design in our lives, we can appreciate the many ways in which it enriches and enhances our experiences.",
    artist: "Greeshma V",
    tag: "greeshma_vinodh",
  },
  jevu: {
    id: 21,
    imgLnk: "images/whitespace_21.png",
    art: "Point of view",
    summary:
      "The world is a complex and diverse place, and that our perception and understanding of it can vary greatly depending on our perspective. Our perspective is the way we see and interpret the world, and it is shaped by our experiences, values, beliefs, and backgrounds. It can influence the way we perceive events, people, and things, and can affect the way we think, feel, and act. Therefore, this design is a reminder that no single perspective is correct or definitive. The world can look very different depending on who is looking at it, and different people can have very different perspectives on the same thing. This is why it is important to consider and respect different perspectives, and to be open to hearing and understanding other people's points of view. By doing so, we can gain a broader and more nuanced understanding of the world, and can enrich our own perspectives and experiences.",
    artist: "Greeshma V",
    tag: "greeshma_vinodh",
  },
  xwdi: {
    id: 22,
    imgLnk: "images/whitespace_22.png",
    art: "Life",
    summary:
      "This is a reminder that the world is a complex and diverse place, and that our perception and understanding of it can vary greatly depending on our perspective. Our perspective is the way we see and interpret the world, and it is shaped by our experiences, values, beliefs, and backgrounds. It can influence the way we perceive events, people, and things, and can affect the way we think, feel, and act.No single perspective is correct or definitive. The world can look very different depending on who is looking at it, and different people can have very different perspectives on the same thing. This is why it is important to consider and respect different perspectives, and to be open to hearing and understanding other people's points of view. By doing so, we can gain a broader and more nuanced understanding of the world, and can enrich our own perspectives and experiences.",
    artist: "Sandra Kannan",
    tag: "szandy8_._",
  },
  byxg: {
    id: 23,
    imgLnk: "images/whitespace_23.png",
    art: "Woman",
    summary:
      "In a world where gender makes no difference, people would be treated equally and without bias regardless of their gender identity. This means that everyone would have the same opportunities in education, employment, and other aspects of society. There would be no stereotypes or assumptions based on gender, and individuals would be able to express themselves and their gender in any way they choose without fear of discrimination. This kind of world would be more inclusive and fair for everyone.",
    artist: "Akshay K A",
    tag: "its_me_akshay_ka",
  },
  zckm: {
    id: 24,
    imgLnk: "images/whitespace_24.png",
    art: "Gender Equality",
    summary:
      "Both men and women should feel free to express their emotions and be sensitive without fear of being judged or labeled. Gender is a spectrum, and individuals can fall anywhere along that spectrum, regardless of the gender they were assigned at birth. There is no one 'right' way to be a man or a woman, and people should be able to express themselves in a way that feels authentic to them. Gender should not be used as a means of limiting or judging individuals, but rather as a way of celebrating the rich diversity of human experience.",
    artist: "Akshay K A",
    tag: "its_me_akshay_ka",
  },
  qsun: {
    id: 25,
    imgLnk: "images/whitespace_25.png",
    art: "Soul",
    summary:
      "It is not possible for anyone to know the inner soul of another person authentically, as the inner soul is a deeply personal and unique aspect of an individual's being. The inner soul is made up of a person's innermost thoughts and feelings, and it is not something that can be known or understood by others. While we may be able to gain some insights into someone's inner soul through their actions, words, and expressions, it is ultimately up to the individual to decide how much of their inner self they want to share with others. It is important to respect people's boundaries and to not try to force them to reveal more of their inner soul than they are comfortable with.",
    artist: "Niranjana Pradeep",
    tag: "niranjana__pradeep",
  },
  nblb: {
    id: 26,
    imgLnk: "images/whitespace_26.png",
    art: "Check Signal Cable ",
    summary:
      "To connect the lines, you will need to make sure that the signal cable is properly connected to the appropriate input and output devices. This will typically involve matching the connectors on the ends of the cable with the corresponding ports on the devices.",
    artist: "Akhil Krishna P",
    tag: "jakeandtheboy",
  },
  trqn: {
    id: 27,
    imgLnk: "images/whitespace_27.png",
    art: "Better Half",
    summary:
      "In general, becoming the colorful half of ourselves means embracing and expressing our individuality and uniqueness. This can involve being open to new experiences and perspectives, trying new things, and allowing ourselves to be who we truly are without fear of judgment or criticism. It is about being true to ourselves and celebrating the diversity and richness of human experience. Becoming the colorful half of ourselves can help us to grow and develop as individuals, and to lead more fulfilling and authentic lives.",
    artist: "Vaishakh Suresh",
    tag: "soulcastor",
  },
  rpmi: {
    id: 28,
    imgLnk: "images/whitespace_28.png",
    art: "Rhythm",
    summary:
      "The faces of the moon refer to the different phases of the moon as it orbits around the Earth. The moon's phases are caused by the changing angle at which the moon is lit by the sun, and they follow a regular and predictable pattern. The most rhythmic manner in which the faces of the moon can be observed is by tracking the moon's phases over time. By doing so, you can see the regular and cyclical changes in the moon's appearance, from the thin crescent of the new moon to the full and bright face of the full moon, and back again.",
    artist: "Muhammed Ali M A",
    tag: "mdali.__",
  },
};
